public class EquationMethods {
    private static final int A = -1;
    private static final int B = 0;
    private static final float EPS = 0.0001f;

    public EquationMethods() {
        System.out.println("f(x) = x^3 + 3 * x + 2");
        System.out.println("f`(x) = 3*x^2 + 3");
        System.out.println("f``(x) = 6x");

        System.out.printf("Метод хорд: %.4f%n", hord());
        System.out.printf("Метод дотичних: %.4f%n", tangent());
        System.out.printf("Комбінований метод: %.4f%n", combined());
        System.out.printf("Метод поділу навпіл: %.4f%n", halfDivision());
        System.out.printf("Метод ітерацій: %.4f%n", iteration());
    }

    /**
     * Метод ітерацій
     */
    private float iteration() {
        float lyambda = -0.2f, c = -0.5f, cN = 0.0f;
        do {
            if (cN != 0.0f) {
                c = cN;
            }

            cN = c + lyambda * getValueOfFunc(c, true);
        } while (Math.abs(cN - c) > EPS);

        return cN;
    }

    /**
     * Метод поділу навпіл
     */
    private float halfDivision() {
        int n = 1_000_000;
        float ak = A, bk = B, cK = 0.0f;
        for (int i = 0; i < n; i++) {
            cK = (ak + bk) / 2;

            if (getValueOfFunc(cK, true) == 0.0f) {
                return cK;
            }

            if (getValueOfFunc(cK, true) * getValueOfFunc(A, true) < 0) {
                bk = cK;
            } else {
                ak = cK;
            }
        }

        return cK;
    }

    /**
     * Комбінований метод
     */
    private float combined() {
        float c0, d0, cN = 0.0f, dN = 0.0f;
        if (sign()) {
            c0 = A;
            d0 = B;
        } else {
            c0 = B;
            d0 = A;
        }

        do {
            if (cN != 0.0f) {
                c0 = cN;
            }
            if (dN != 0.0f) {
                d0 = dN;
            }

            cN = c0 - (getValueOfFunc(c0, true) * (d0 - c0)) / (getValueOfFunc(d0, true) - getValueOfFunc(c0, true));
            cN = (float) Math.round(cN * 10_000) / 10_000;
            dN = d0 - getValueOfFunc(d0, true) / getValueOfFunc(d0, false);
            dN = (float) Math.round(dN * 10_000) / 10_000;
        } while (Math.abs(cN - dN) > EPS);

        return (cN + dN) / 2;
    }

    /**
     * Метод хорд
     */
    private float hord() {
        float c, b, cN = 0.0f;
        if (sign()) {
            c = A;
            b = B;
        } else {
            c = B;
            b = A;
        }

        do {
            if (cN != 0.0f) {
                c = cN;
            }

            cN = c - (getValueOfFunc(c, true) * (b - c)) / (getValueOfFunc(b, true) - getValueOfFunc(c, true));
        } while (Math.abs(c - cN) > EPS);

        return cN;
    }

    /**
     * Метод дотичних
     */
    private float tangent() {
        float d0, dN = 0.0f;

        if (sign()) {
            d0 = B;
        } else {
            d0 = A;
        }

        do {
            if (dN != 0.0f) {
                d0 = dN;
            }

            dN = d0 - getValueOfFunc(d0, true) / getValueOfFunc(d0, false);
            dN = (float) Math.round(dN * 10_000) / 10_000;
        } while (Math.abs(dN - d0) > EPS);

        return dN;
    }

    private boolean sign() {
        return (3 * Math.pow((float) -0.5, 2) + 3) * 6 * (float) -0.5 > 0;
    }

    private float getValueOfFunc(float x, boolean func) {
        return func ? (float) (Math.pow(x, 3) + 3 * x + 2)
                : (float) (3 * Math.pow(x, 2) + 3);
    }
}
